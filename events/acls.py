import requests
import json
from .keys import PEXELS_API_KEY


def get_photo(city, state):
    # print("PEXEL KEY", PEXELS_API_KEY)
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + " " + state, "per_page": 1}
    response = requests.get(url, params=params, headers=headers)
    # print(response)
    content = response.json()
    # print("*****************", content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


# print(get_photo("Pasadena", "CA"))


# def get_weather_data(city, state):
#     # Create the URL for the geocoding API with the city and state
#     url = 'https://api.openweathermap.org/geo/1.0/direct'
#     headers = {"Authorization": OPEN_WEATHER_API_KEY}
#     params = {"query": city}
#     # Make the request
#     response = requests.get(url, params=params, headers=headers)
#     # Parse the JSON response
#     content = response.json()
#     # Get the latitude and longitude from the response
#     if content:
#         latitude = content[0]["lat"]
#         longitude = content[0]["lon"]
#     # Create the URL for the current weather API with the latitude
#     weather_url=https://api.openweathermap.org/data/2.5/weather?lat=44.34&lon=10.99&appid={OPEN_WEATHER_API_KEY}
#     #   and longitude
#     # Make the request
#     # Parse the JSON response
#     # Get the main temperature and the weather's description and put
#     #   them in a dictionary
#     # Return the dictionary
# def get_weather_data(city, state):
#     # Geocoding API URL
#     geocoding_url = "https://api.openweathermap.org/geo/1.0/direct"
#     geocoding_headers = {"Authorization": OPEN_WEATHER_API_KEY}
#     geocoding_params = {"q": f"{city},{state}", "limit": 1}

#     # Make the request to geocoding API
#     geocoding_response = requests.get(
#         geocoding_url, params=geocoding_params, headers=geocoding_headers
#     )
#     if geocoding_response.status_code != 200:
#         return None  # Request failed, handle error here
#     print(geocoding_response)
#     # Parse JSON response
#     geocoding_data = geocoding_response.json()

#     # Get latitude and longitude
#     if geocoding_data:
#         latitude = geocoding_data[0]["lat"]
#         longitude = geocoding_data[0]["lon"]
#     else:
#         return None  # No data found or invalid response

#     # Current weather API URL using obtained latitude and longitude
#     weather_url = "https://api.openweathermap.org/data/2.5/weather"
#     weather_header = {"Authorization": OPEN_WEATHER_API_KEY}
#     weather_params = {
#         "lat": latitude,
#         "lon": longitude,
#         "units": "metric",
#     }

#     # Make the request to current weather API
#     weather_response = requests.get(
#         weather_url, params=weather_params, header=weather_header
#     )
#     if weather_response.status_code != 200:
#         return None  # Request failed, handle error here
#     print(weather_response)
#     # Parse JSON response
#     weather_data = weather_response.json()

#     # Get main temperature and weather description
#     if weather_data:
#         main_temperature = weather_data["main"]["temp"]
#         weather_description = weather_data["weather"][0]["description"]

#         # Store in a dictionary
#         weather_dict = {
#             "temperature": main_temperature,
#             "description": weather_description,
#         }
#         return weather_dict
#     else:
#         return None  # No data found or invalid response


# print(get_weather_data("Pasadena", "CA"))
